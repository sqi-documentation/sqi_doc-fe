import React, { useEffect, useState } from 'react'
import { Redirect } from 'react-router'

import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import { EditorState, convertToRaw, convertFromRaw } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import draftToHtml from 'draftjs-to-html'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

import * as MdIcons from 'react-icons/md'
import * as RiIcons from 'react-icons/ri'
import * as ImIcons from 'react-icons/im'

import {
    CCol, CRow, CCollapse, CTooltip,
    CCard, CCardHeader, CCardBody, CCardTitle,
    CFormGroup, CInput, 
    CButton,
    CModal, CModalHeader, CModalBody, CModalFooter, CModalTitle
} from '@coreui/react'

import Api from './../../../service/Api'
import AuthService from 'src/service/AuthService'
import IdleTimerHandler from 'src/service/IdleTimer'
import Notifications from 'src/utils/Notification'

import MaterialTable from 'material-table'
import './Wiki.css'

const WikiViewAllGuides = () => {

    // Columns for Audit Log Table
    const columns = [
        { 
            title: "Guide Title", 
            field: "entityKey", 
            sorting: false,
            headerStyle: {
                textAlign: "center"
            },  
        },
        { 
            title: "Perfomed Action", 
            field: "performedAction", 
            sorting: false,
            headerStyle: {
                textAlign: "center"
            },
            cellStyle: {
                textAlign: "center"
            } 
        },
        { 
            title: "Perfomed Date", 
            field: "performedDate", 
            defaultSort: "desc",
            headerStyle: {
                textAlign: "center"
            },
            cellStyle: {
                textAlign: "center"
            }
        },
        { 
            title: "Perfomed By", 
            field: "performedBy", 
            sorting: false,
            headerStyle: {
                textAlign: "center"
            },
            cellStyle: {
                textAlign: "center"
            } 
        },
        { 
            title: "New Value", 
            field: "newValue", 
            emptyValue: () => <em>null</em>, 
            sorting: false, 
            render: rowData => convertFromHtml(rowData.newValue),
            headerStyle: {
                textAlign: "center"
            } 
        },
        { 
            title: "Old Value", 
            field: "oldValue", 
            emptyValue: () => <em>null</em>, 
            sorting: false, 
            render: rowData => convertFromHtml(rowData.oldValue),
            headerStyle: {
                textAlign: "center"
            } 
        }
    ]

    // STATE ====================================================================
    // State for Log
    const [logs, setLogs] = useState([])
    
    // State for Guide
    const [guides, setGuides] = useState([])
    const [title, setTitle] = useState("")
    const [contents, setContents] = useState(() => EditorState.createEmpty())
    
    // State for Search Guide
    const [search, setSearch] = useState("")

    // State for Modal
    const [edit, setEdit] = useState(false)
    const [showLog, setShowLog] = useState(false)
    const [showConfirmation, setShowConfirmation] = useState(false)

    // State for Expand & Collapse Guide Details
    const [isAccordionOpen, setIsAccordionOpen] = useState(null)

    // State for Identify Object (whether it's guide or attachment)
    const [object, setObject] = useState("")
    const [deletedObject, setDeletedObject] = useState("")

    // API SERVICE ==============================================================
    // GET - Log
    const retrieveLogs = () => {
        Api.get("/log/guide")
        .then((response) => {
            setLogs(response.data)
        }).catch((err) => {
            Notifications.error("RETRIEVE LOGS FAILED", `Error while retrieving logs:\n${err}`)
        })
    }

    // GET - Guide
    const retrieveGuides = () => {
        Api.get("/guide")
        .then((response) => {
            setGuides(response.data)
        }).catch((err) => {
            Notifications.error("RETRIEVE GUIDES FAILED", `Error while retrieving guides:\n${err}`)
        })
    }

    // DELETE - Guide
    const deleteGuide = (title) => {
        Api.delete(`/guide/${title}`)
        .then((response) => {
            Notifications.success("DELETE GUIDE SUCCESS", `Successfully delete guide with title "${title}"!`)
            setObject("")
            setDeletedObject("")
            retrieveGuides()
            retrieveLogs()
        }).catch((err) => {
            Notifications.error("DELETE GUIDE FAILED", `Error occured while deleting guide:\n"${err}"`)
        })
        setShowConfirmation(!showConfirmation)
    }

    // PUT - Guide
    const editGuide = () => {
        let content = contents.getCurrentContent()
        const request = {
            title: title.trim(),
            contentPlainText: content.getPlainText(),
            contentBlocks: JSON.stringify(convertToRaw(content)),
            contentHtml: draftToHtml(convertToRaw(content))
        }
        Api.put(`/guide/${title}`, request)
        .then((response) => {
            Notifications.success("EDIT GUIDE SUCCESS", `Successfully edit guide with title "${title}"!`)
            resetInput()
            retrieveGuides()
            retrieveLogs()
        }).catch((err) => {
            Notifications.error("EDIT GUIDE FAILED", `Error occured while saving edited guide:\n"${err}"`)
        })
        setEdit(!edit)
    }

    // FUNCTION =================================================================
    const resetInput = () => {
        setTitle("")
        setContents(() => EditorState.createEmpty())
    }

    const onConfirmDelete = (object, id) => { 
        setObject(object)
        setDeletedObject(id) 
        setShowConfirmation(!showConfirmation)
    }

    const onEditGuide = (index) => {
        setTitle(guides[index].title)
        setContents(EditorState.createWithContent(convertFromRaw(JSON.parse(guides[index].contentBlocks))))
        setEdit(!edit)
    }

    const extendAcordion = (index) => {
        if(isAccordionOpen == index) {
            return setIsAccordionOpen(null)
        }
        setIsAccordionOpen(index)
    }

    const convertFromHtml =(e) => {
        return <div dangerouslySetInnerHTML={{__html: e}}/>
    }

    const onSubmit = (e) => {
        if(e.target.name === "edit_guide") {
            if (contents.getCurrentContent().getPlainText().length == 0) {
                Notifications.error("EDIT GUIDE FAILED", "Content cannot be empty!")
            }
            else editGuide()
        } else if(e.target.name === "delete_guide") {
            deleteGuide(deletedObject)
        }
    }

    useEffect(() => {
        const user = AuthService.getCurrentUser()
        if(user == null) return( <Redirect to="/login" /> )
        else {
            const userRole =  AuthService
                                .getCurrentUser("User")
                                .user.role.map((role, index) => {
                                    return role.roleName
                                })
            if(userRole != "Admin") return ( <Redirect to="/404" /> )
            else {
                retrieveGuides()
                retrieveLogs()
            }
        }
    }, [])

    return (
        <>
            <IdleTimerHandler />
            <>
                <CRow>
                    <CCol className="w-100">
                        <CInput className="form-control m-0 mb-2"
                            type="text" 
                            value={search} 
                            placeholder="Search guide ..."
                            onChange={(e) => setSearch(e.target.value)}
                            style={{width: "40%"}} 
                        />
                    </CCol>
                    <div className="m-0 p-0 text-right mr-4 my-1">
                        <CButton className="btn-sm btn-warning" 
                            style={{color: "white"}} 
                            onClick={() => setShowLog(!showLog)}>
                            Audit Log
                        </CButton>
                    </div>
                </CRow>
                <hr />

                {/* === List of Guides === */}
                {
                    guides.filter((guide, index) => {
                        if(search=="") {
                            return guide
                        } else if (guide.title.toLowerCase().includes(search.toLowerCase())) {
                            return guide
                        }
                    }).map((guide, index) => {
                        return (
                            <CCol className="accordion p-0" key={guide.title} xs="12" sm="12" md="12">
                                <CCard className="w-100 px-2">
                                    <CCardHeader className="accordion-header row">
                                        <CCol className="p-0">
                                            <CCardTitle className="m-1 ml-2"
                                                onClick={() => extendAcordion(guide.title)}
                                                style={{fontSize: "17px", cursor:"pointer"}}>
                                                {guide.title}
                                            </CCardTitle>
                                        </CCol>
                                        <CCol className="p-0 text-right">
                                            {/* <CTooltip content="Attachments" placement="bottom-start">
                                                <RiIcons.RiAttachment2 className="m-1" 
                                                    onClick={() => showAttachForm()}
                                                    style={{cursor: "pointer"}} 
                                                />
                                            </CTooltip> */}
                                            <CTooltip content="Edit" placement="bottom-start">
                                                <MdIcons.MdEdit className="m-1" 
                                                    onClick={() => onEditGuide(index)}
                                                    style={{cursor: "pointer"}} 
                                                />
                                            </CTooltip>
                                            <CTooltip content="Delete" placement="bottom-start">
                                                <ImIcons.ImBin className="m-1" 
                                                    onClick={() => onConfirmDelete("guide", guide.title)} 
                                                    style={{cursor: "pointer"}}
                                                />
                                            </CTooltip>
                                            {
                                                isAccordionOpen == guide.title ? 
                                                <RiIcons.RiArrowUpSLine className="m-1" 
                                                    onClick={() => extendAcordion(guide.title)} 
                                                    style={{cursor: "pointer"}}
                                                /> :
                                                <RiIcons.RiArrowDownSLine className="m-1" 
                                                    onClick={() => extendAcordion(guide.title)} 
                                                    style={{cursor: "pointer"}}
                                                />
                                            }
                                        </CCol>
                                    </CCardHeader>
                                    <CCollapse show={isAccordionOpen == guide.title}>
                                        <CCardBody className={isAccordionOpen == guide.title ? 'content show' : 'content'}>
                                            <Editor
                                                editorState={EditorState.createWithContent(convertFromRaw(JSON.parse(guide.contentBlocks)))}
                                                readOnly={true}
                                                toolbarHidden={true}
                                            />
                                        </CCardBody>
                                    </CCollapse>
                                </CCard>
                            </CCol>
                        )
                    })
                }

                {/* === Modal for Edit Guide === */}
                <CModal size="xl" show={edit} scrollable={true} closeOnBackdrop={false}>
                    <CModalHeader>
                        <CModalTitle>Edit Guide</CModalTitle>
                    </CModalHeader>
                    <CModalBody>
                        <CFormGroup>
                            <CInput className="form-control" 
                                type="text" 
                                value={title} 
                                readOnly
                            />
                        </CFormGroup>
                        <CFormGroup>
                            <Editor
                                editorState={contents}
                                toolbarClassName="toolbar-class"
                                wrapperClassName="wrapper-class"
                                editorClassName="editor-class"
                                placeholder=" Write guide content ..."
                                onEditorStateChange={setContents}
                            />
                        </CFormGroup>
                    </CModalBody>
                    <CModalFooter>
                        <CButton name="edit_guide" color="primary" onClick={onSubmit}>Edit</CButton>
                        <CButton color="secondary" onClick={() => setEdit(false)}>Cancel</CButton>
                    </CModalFooter>
                </CModal>

                {/* === Modal for Action Confirmation === */}
                <CModal show={showConfirmation} closeOnBackdrop={false}>
                {
                    object == "guide" ?
                    <>
                        <CModalHeader>
                            <CModalTitle>Delete Guide</CModalTitle>
                        </CModalHeader>
                        <CModalBody>
                            <p>Are you sure you want to delete this guide?</p>
                        </CModalBody>
                        <CModalFooter>
                            <CButton name="delete_guide" color="success" onClick={onSubmit}>Yes</CButton>
                            <CButton color="danger" onClick={() => setShowConfirmation(false)}>No</CButton>
                        </CModalFooter>
                    </> : <></>
                }
            </CModal>

            {/* === Modal for Audit Log === */}
            <CModal size="lg" show={showLog} onClose={() => setShowLog(false)} >
                <CModalHeader closeButton>
                    <CModalTitle>Audit Log</CModalTitle>
                </CModalHeader>
                <CModalBody className="px-4">
                   {
                        logs.length > 0 ?
                        <>
                        <MaterialTable
                            columns={columns} 
                            data={logs} 
                            options={{ 
                                showTitle: false,
                                sorting: true,
                                search: true,
                                searchFieldAlignment: "left",
                                searchAutoFocus: true,
                                searchFieldVariant: "standard",
                                filtering: false,
                                paging: true,
                                pageSizeOptions: [5, 10, 20, 25],
                                pageSize: 5,
                                paginationType: "normal",
                                exportButton: true, 
                                exportAllData: true,
                                exportFileName: "[GUIDE]Audit_Log",
                                headerStyle: {
                                    fontWeight: "bold",
                                },
                                rowStyle: (log, index) => index%2 == 0 ? {background: "#f5f5f5"} : null,
                            }}
                        />
                        </>  : <p className="text-center">No log yet</p>
                    }
                </CModalBody>
            </CModal>
            </>

            {/* === Container for Notification === */}
            <ToastContainer />
        </>
    )
}

export default WikiViewAllGuides
