import React, { Component } from 'react'

import {
  CProgress,
  CLink,
  CTooltip
} from  '@coreui/react'
import CIcon from '@coreui/icons-react'

import FileService from 'src/service/FileService';

class UploadComponent extends Component {
    
    _isMounted = false

    constructor(props) {
        super(props);

        this.state = {
          selectedFiles: undefined,
          currentFile: undefined,
          progress: 0,
          message: "",
          search: "",
    
          files: [],
        };
    }

    selectFile(event) {
        this.setState({
          selectedFiles: event.target.files,
          message: "",
          progress: 0
        });
    }

    searchFile(event) {
        this.setState({
          search: event.target.value
        });
    }

    upload() {
        let currentFile = this.state.selectedFiles[0];
        
        this.setState({
          progress: 0,
          currentFile: currentFile,
        });
        FileService.upload(currentFile, (event) => {
          this.setState({
            progress: Math.round((100 * event.loaded) / event.total)
          })
        })
          .then((response) => {
            this.setState({
              message: response.data.message,
            });
            return FileService.getFiles();
          })
          .then((files) => {
            this.setState({
              files: files.data,
            });
          })
          .catch(() => {
            this.setState({
              progress: 0,
              message: "Could not upload the file!",
              currentFile: undefined,
            });
            return
          });
        this.setState({
          selectedFiles: undefined
        });
    }

    componentDidMount() {

        this._isMounted = true;

        FileService.getFiles().then((response) => {
          if(this._isMounted) {
            this.setState({
                files: response.data,
              });
          }
        })
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {
        const {
          selectedFiles,
          currentFile,
          progress,
          message,
          files,
          search
        } = this.state;
    
        return (
          <div>
            {currentFile && progress > 0 && (
            //   <div className="progress">
            //     <div
            //       className="progress-bar progress-bar-info progress-bar-striped"
            //       role="progressbar"
            //       aria-valuenow={progress}
            //       aria-valuemin="0"
            //       aria-valuemax="100"
            //       style={{ width: progress + "%" }}
            //     >
            //       {progress}%
            //     </div>
            //   </div>
                <div className="row mb-2">
                    <div className="col">
                        {/* <ProgressBar now={progress} striped={true} label={`${progress}%`} /> */}
                        <CProgress color="info" animated showPercentage precision={0} value={progress}/>
                    </div>
                </div>
             )}
    
            <div className="row">
                <div className="col">
                    <CTooltip
                      content="Upload file max. 200MB"
                      placement="bottom-start"
                    >
                      <label className="btn btn-default px-0">
                          <input type="file" name="Max. 200MB" onChange={(e) => this.selectFile(e)} />
                      </label>
                    </CTooltip>
                </div>
        
                <div className="col-2 text-right">
                    <button className="btn btn-primary"
                        disabled={!selectedFiles}
                        onClick={(e) => this.upload(e)}
                        > Attach
                    </button>
                </div>
            </div>
    
            {
                message.length > 0 ?
                <>
                    {
                        message.startsWith("Could") ? 
                        <div className="alert alert-danger" role="alert">
                            <p className="my-1">{message}</p>
                        </div>:
                        <div className="alert alert-success" role="alert">
                            <p className="my-1">{message}</p>
                        </div>
                    }
                </>
                :<></>
            }

            <hr />
    
            <div className="card">
                <div className="card-header">
                    <div className="row">
                        <div className="col d-flex align-items-center">
                            <h4 className="card-title m-0">List of Templates</h4>
                        </div>
                        <div className="col d-flex justify-content-end my-1 mr-1">
                            <input type="text" value={search} placeholder="Search template..." className="form-control w-50" onChange={(e) => this.searchFile(e)} />
                        </div>
                    </div>
                </div>
              <ul className="list-group list-group-flush">
                {files &&
                  files.filter((file, index) => {
                    if(search=="") {
                        return file
                    } else if (file.name.toLowerCase().includes(search.toLowerCase())) {
                        return file
                    }
                }).map((file, index) => (
                    <li className="list-group-item d-flex align-items-center" key={index}>
                        {/* <div>
                          <p className="m-0"
                              style={{cursor: "pointer"}} 
                              // onClick={(e) => this.download(file.id, file.url, file.name, file.type)}>
                              onClick={(e) => this.download(file.name)}>
                              {file.name}
                          </p>
                          <hr className="m-0 p-0 mx-1"/>
                            <p>{file.url}</p>
                            <p>{file.extension}</p>
                            <p>{file.byteSize}</p>
                            <p>{file.filePath}</p>
                            <p>{file.contentDisposition}</p>
                            <p>{file.contentType}</p>
                          </div> */}
                          
                          <CLink className="card-header-action" name="download_attach" href={file.url} target="_blank">
                            <CIcon name="cil-cloud-download"/>{' '}
                            {' '}{file.name}
                          </CLink>
                        {/* <a href={file.url} download="">
                            <p className="card-text m-0 ml-2">{file.name}</p>
                        </a> */}
                    </li>
                  ))}
              </ul>
            </div>
          </div>
        );
      }
    }

export default UploadComponent
