import { Redirect } from 'react-router'

import AuthService from 'src/service/AuthService'
import IdleTimerHandler from 'src/service/IdleTimer';
import UploadComponent from './UploadComponent';

const WikiExploreRequestTemplate = () => {

    if(AuthService.getCurrentUser() == null) {
        return( <Redirect to="/login" /> )
    }

    const who = AuthService.getCurrentUser("User").user.role.map((role, index) => {
        return role.roleName
    })

    // console.log("Current user role  [Wiki-Request]: " + who)
    
    if(who == "User") {
        return ( <Redirect to="/404" /> )
    }

    return (
        <>
            <IdleTimerHandler />
            <>
                <UploadComponent />
            </>
            {/* <ToastContainer autoClose={3000} draggable={false} closeButton={false} transition={Slide}/> */}
        </>
    )
}

export default WikiExploreRequestTemplate
