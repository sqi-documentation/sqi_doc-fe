import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router'

import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import { EditorState, convertToRaw } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import draftToHtml from 'draftjs-to-html'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

import {
    CCol,
    CFormGroup, CInput, 
    CButton,
    CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle
} from '@coreui/react'

import Api from './../../../service/Api'
import AuthService from 'src/service/AuthService'
import IdleTimerHandler from 'src/service/IdleTimer'
import Notifications from 'src/utils/Notification'

import './Wiki.css'

const Wiki = () => {

    // STATE ====================================================================
    // State for Action Confirmation
    const [showConfirmation, setShowConfirmation] = useState(false)

    // State for Guide
    const [title, setTitle] = useState("")
    const [contents, setContents] = useState(() => EditorState.createEmpty())

    // API SERVICE ==============================================================
    // POST - Create Guide
    const createGuide = () => {

        let content = contents.getCurrentContent()
        const request = {
            title: title.trim(),
            contentPlainText: content.getPlainText(),
            contentBlocks: JSON.stringify(convertToRaw(content)),
            contentHtml: draftToHtml(convertToRaw(content))
        }
        // Find by ID first to check guide with that title exists or not
        Api.get(`/guide/${title}`)
        .then((isExists) => {
            if(isExists.data != null) {
                Notifications.error("CREATE GUIDE FAILED", `Guide with title "${title}" already exists!`)
            } else {
                Api.post('/guide', request)
                .then((response) => {
                        Notifications.success("CREATE GUIDE SUCCESS", `Successfully create guide with title "${title}"!`)
                        resetInput()
                }).catch((err) => {
                    Notifications.error("CREATE GUIDE FAILED", `Error occured while creating guide:\n${err}`)
                })
            }
        }).catch((err) => {
            Notifications.error("CREATE GUIDE FAILED", `Something's wrong:\n${err}`)
        })
    }

    // FUNCTION =================================================================
    const resetInput = () => {
        setTitle("")
        setContents(() => EditorState.createEmpty())
    }

    const onSubmit = (e) => {
        if(title.length == 0 || contents.getCurrentContent().getPlainText().length == 0) {
            Notifications.error("CREATE GUIDE FAILED", "Title and content cannot be empty!")
        } else createGuide()
        setShowConfirmation(!showConfirmation)
    }

    useEffect(() => {
        const user = AuthService.getCurrentUser()
        if(user == null) return( <Redirect to="/login" /> )
        else {
            const userRole =  AuthService
                                .getCurrentUser("User")
                                .user.role.map((role, index) => {
                                    return role.roleName
                                })
            if(userRole != "Admin") return ( <Redirect to="/404" /> )
        }
    }, [])

    return (
        <>
            <IdleTimerHandler />

            {/* === Component for Create New Guide === */}
            <CCol className="card p-4 m-0">
                <CFormGroup>
                    <CInput className="form-control card-header" 
                        type="text" 
                        value={title} 
                        placeholder="Guide Title" 
                        onChange={(e) => setTitle(e.target.value)}
                        required
                    />
                </CFormGroup>
                <CFormGroup className="editor">
                    <Editor
                        editorState={contents}
                        toolbarClassName="toolbar-class"
                        wrapperClassName="wrapper-class"
                        editorClassName="editor-class"
                        placeholder=" Write guide content ..."
                        onEditorStateChange={setContents}
                    />
                </CFormGroup>
                <CFormGroup className="card-footer m-0 p-0 d-flex justify-content-end">
                    <CButton className="mt-3" 
                        color="primary" 
                        onClick={() => setShowConfirmation(!showConfirmation)}>
                        Add
                    </CButton>
                </CFormGroup>
            </CCol>

            {/* === Modal for Action Confirmation === */}
            <CModal show={showConfirmation} closeOnBackdrop={false}>
                <CModalHeader>
                    <CModalTitle>Add New Guide</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <p>Are you sure you want to add this guide?</p>
                </CModalBody>
                <CModalFooter>
                    <CFormGroup>
                        <CButton color="success" onClick={onSubmit}>Yes</CButton>{'   '}
                        <CButton color="danger" onClick={() => setShowConfirmation(false)}>No</CButton>
                    </CFormGroup>
                </CModalFooter>
            </CModal>

            {/* === Container for Notification === */}
            <ToastContainer />
        </>
    )
}

export default Wiki