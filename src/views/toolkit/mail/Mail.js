import { React, useState } from 'react'
import { Redirect } from 'react-router'
import { ToastContainer, toast, Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import emailjs from 'emailjs-com'

import ReactQuill from 'react-quill'
import "react-quill/dist/quill.snow.css";

import { 
    CFade,
    CHeaderNav,
    CHeaderNavLink,
    CForm,
    CLabel,
    CInput,
    CTextarea
 } from '@coreui/react'

import BCAlogo from './../../../assets/images/logo-bca.png'
import * as FaIcon from 'react-icons/fa'

import TheFooter from './../../../containers/TheFooter'
import AuthService from 'src/service/AuthService'
import IdleTimerHandler from 'src/service/IdleTimer'

const modules = {
    toolbar: {
        container: [
            [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
            [{ 'font': [] }],
            [{ 'header': 1 }, { 'header': 2 }],               // custom button values
            ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
            [{ 'align': [] }],
            [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
            [{ 'direction': 'rtl' }],                         // text direction
            [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
            ['blockquote', 'code-block'],
        
            [{ 'list': 'ordered' }, { 'list': 'bullet' }],
            [{ 'color': [] }, { 'background': [] }],
            ['emoji', 'image', 'video', 'link'],
        
            ['clean'],
        ]
    },
}

const Mail = () => {

    const [destination, setDestination] = useState("")
    const [subject, setSubject] = useState("")
    const [message, setMessage] = useState("")

    const onChangeDestination = (e) => { setDestination(e.target.value) }
    const onChangeSubject = (e) => { setSubject(e.target.value) }
    const onChangeMessage = (e) => { setMessage(e) }

    const sendEmail = (e) => {
        e.preventDefault()

        if(destination == "") {
            toast.error("Email destination cannot be empty!", {
                position: toast.POSITION.BOTTOM_RIGHT,
                closeOnClick: false,
                pauseOnHover: false
            })
            return
        }

        let serviceId = 'toolkit_test_gmail'
        let templateId = 'template_gmail'
        let userId = 'user_LbqlYpA8lWqlo7DhNtFhp'

        emailjs.sendForm(serviceId, templateId, e.target, userId)
            .then((res) => {
                console.log(res.text)
            }).catch((err) => {
                console.log(err.text)
            })

        console.log(destination)
        console.log(subject)
        console.log(message)
        e.target.reset()
    }

    const convert = (e) => {
        return <div dangerouslySetInnerHTML={{__html: e}}/>
    }

    // console.log('mail:' + AuthService.getCurrentUser())
    console.log('mail:' + JSON.stringify(AuthService.getCurrentUser()))

    if(AuthService.getCurrentUser() == null) {
        return(
            <Redirect to="/login" />
        )
    }

    const who = AuthService.getCurrentUser("User").user.email

    return (
        <>
        <IdleTimerHandler />
        <CFade>
            <div className="c-app c-default-layout">
                <div className="c-wrapper">
                    <header className="bg-white shadow d-flex align-items-center" style={{height: "5rem"}}>
                        <img src={BCAlogo} className="ml-2" style={{height: "180%"}} />
                        <div className="mfs-auto">
                            <CHeaderNav className="px-3">
                                <CHeaderNavLink to="/">
                                    <div className="border px-2 py-1 rounded">
                                        <FaIcon.FaHome color="rgba(0, 0, 21, 0.5)"/>
                                    </div>
                                </CHeaderNavLink>
                            </CHeaderNav>
                        </div>
                    </header>
                    <div className="c-body justify-content-center align-items-center">
                        <div className="container-fluid card p-4 m-4 w-75">
                            <CForm className="card-body p-0 pt-4 mx-3" onSubmit={sendEmail.bind(this)}>
                                <div className="mb-3 m-0 d-flex align-items-center row w-100">
                                    <CLabel className="col-2 m-0 p-0">From</CLabel>
                                    <CInput
                                        className="col"
                                        type="text"
                                        name="sender"
                                        value={who}
                                        readOnly
                                    />
                                </div>
                                <div className="mb-3 m-0 d-flex align-items-center row w-100">
                                    <CLabel className="col-2 m-0 p-0">To</CLabel>
                                    <CInput
                                        className="col"
                                        type="text"
                                        name="destination"
                                        placeholder="Email destination"
                                        onChange={onChangeDestination.bind(this)}
                                    />
                                </div>
                                <div className="mb-3 d-flex align-items-center">
                                    <CInput
                                        type="text"
                                        name="subject"
                                        placeholder="Subject"
                                        onChange={onChangeSubject.bind(this)}
                                    />
                                </div>
                                <div className="mb-3">
                                    {/* <ReactQuill 
                                        name="message"
                                        placeholder="Compose email"
                                        onChange={onChangeMessage.bind(this)}
                                        theme="snow" 
                                        modules={modules}
                                    /> */}
                                    <CTextarea
                                        type="text"
                                        name="message"
                                        placeholder="Compose email"
                                        onChange={onChangeMessage.bind(this)}
                                    />
                                </div>
                                <div className="mfs-auto" style={{width:"10%"}}>
                                    <CInput 
                                        type="submit"
                                        className="my-1 mt-2" 
                                        value="Send"
                                        style={{backgroundColor:"rgb(50, 31, 219)",color:"white"}} />
                                </div>
                            </CForm>        
                        </div>
                    </div>
                    <TheFooter />
                </div>
            </div> 
            <ToastContainer autoClose={3000} draggable={false} closeButton={false} transition={Slide}/>  
        </CFade> 
        </>
    )
}

export default Mail
