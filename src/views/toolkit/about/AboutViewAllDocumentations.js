import React, { useEffect, useState } from 'react'
import { Redirect } from 'react-router'

import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import { EditorState, convertToRaw, convertFromRaw } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import draftToHtml from 'draftjs-to-html'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

import * as MdIcons from 'react-icons/md'
import * as RiIcons from 'react-icons/ri'
import * as ImIcons from 'react-icons/im'

import {
    CContainer, CCol, CRow, CCollapse, CTooltip,
    CCard, CCardHeader, CCardBody, CCardTitle,
    CFormGroup, CInput, 
    CButton, CSelect,
    CModal, CModalHeader, CModalBody, CModalFooter, CModalTitle
} from '@coreui/react'

import Api from '../../../service/Api'
import AuthService from 'src/service/AuthService'
import IdleTimerHandler from 'src/service/IdleTimer'
import Notifications from 'src/utils/Notification'

import MaterialTable from 'material-table';
import './About.css'

const AboutViewAllDocumentations = () => {

    // Columns for Audit Log Table
    const columns = [
        { 
            title: "Name", 
            field: "entityKey", 
            sorting: false,
            headerStyle: {
                textAlign: "center"
            },  
        },
        { 
            title: "Perfomed Action", 
            field: "performedAction", 
            sorting: false,
            headerStyle: {
                textAlign: "center"
            },
            cellStyle: {
                textAlign: "center"
            }
        },
        { 
            title: "Perfomed Date", 
            field: "performedDate", 
            defaultSort: "desc",
            headerStyle: {
                textAlign: "center"
            },
            cellStyle: {
                textAlign: "center"
            }
        },
        { 
            title: "Perfomed By", 
            field: "performedBy", 
            sorting: false,
            headerStyle: {
                textAlign: "center"
            },
            cellStyle: {
                textAlign: "center"
            } 
        },
        { 
            title: "New Value", 
            field: "newValue", 
            emptyValue: () => <em>null</em>, 
            sorting: false, 
            render: rowData => convertFromHtml(rowData.newValue),
            headerStyle: {
                textAlign: "center"
            } 
        },
        { 
            title: "Old Value", 
            field: "oldValue", 
            emptyValue: () => <em>null</em>, 
            sorting: false, render: 
            rowData => convertFromHtml(rowData.oldValue),
            headerStyle: {
                textAlign: "center"
            } 
        }
    ]

    // STATE ====================================================================
    // State for Log
    const [logs, setLogs] = useState([])
    
    // State for Teams
    const [teams, setTeams] = useState([])
    const [teamName, setTeamName] = useState("")
    const [teamDesc, setTeamDesc] = useState(() => EditorState.createEmpty())
    const [selectedTeam, setSelectedTeam] = useState("")
    
    // State for Applications
    const [applications, setApplications] = useState([])
    const [applicationName, setApplicationName] = useState("")
    const [applicationDesc, setApplicationDesc] = useState(() => EditorState.createEmpty())

    // State for Search Applications
    const [search, setSearch] = useState("")
    
    // State for Modal
    const [edit, setEdit] = useState(false)
    const [showLog, setShowLog] = useState(false)
    const [showConfirmation, setShowConfirmation] = useState(false)

    // State for Expand & Collapse Application Details
    const [isAccordionOpen, setIsAccordionOpen] = useState(null)
    
    // State for Identify Object (whether it's team or application)
    const [object, setObject] = useState("")
    const [deletedObject, setDeletedObject] = useState("")

    // API SERVICE ==============================================================
    // GET = Logs
    const retrieveLogs = () => {
        Api.get("/log/team-app")
        .then((response) => {
            setLogs(response.data)
        }).catch((err) => {
            Notifications.error("RETRIEVE LOGS FAILED", `Error while retrieving logs:\n${err}`)
        })
    }

    // GET - Team
    const retrieveTeams = () => {
        Api.get("/team")
        .then((response) => {
            setTeams(response.data)
        }).catch((err) => {
            Notifications.error("RETRIEVE TEAMS FAILED", `Error while retrieving teams:\n${err}`)
        })
    }

    // DELETE - Team
    const deleteTeam = (name) => {
        Api.delete(`/team/${name}`)
        .then((response) => {
            Notifications.success("DELETE TEAM SUCCESS", `Successfully delete team with name "${name}"!`)
            setDeletedObject("")
            retrieveTeams()
            retrieveLogs()
        }).catch((err) => {
            Notifications.error("DELETE TEAM FAILED", `Error occured while deleting team:\n"${err}"`)
        })
        setShowConfirmation(!showConfirmation)
    }

    // PUT - Team
    const editTeam = () => {
        let content = teamDesc.getCurrentContent()
        const request = {
            name: teamName.trim(),
            descriptionPlainText: content.getPlainText(),
            descriptionBlocks: JSON.stringify(convertToRaw(content)),
            descriptionHtml: draftToHtml(convertToRaw(content))
        }
        Api.put(`/team/${teamName}`, request)
        .then((response) => {
            Notifications.success("EDIT TEAM SUCCESS", `Successfully edit team with name "${teamName}"!`)
            resetInput()
            retrieveTeams()
            retrieveLogs()
        }).catch((err) => {
            Notifications.error("EDIT TEAM FAILED", `Error occured while saving edited team:\n"${err}"`)
        })
        setEdit(!edit)
    }

    // GET - Applications
    const retrieveApplications = () => {
        Api.get("/app")
        .then((response) => {
            setApplications(response.data)
        }).catch((err) => {
            Notifications.error("RETRIEVE APPLICATIONS FAILED", `Error while retrieving applications:\n${err}`)
        })
    }

    // DELETE - Applications
    const deleteApplication = (name) => {
        Api.delete(`/app/${name}`)
        .then((response) => {
            Notifications.success("DELETE APPLICATION SUCCESS", `Successfully delete application with name "${name}"!`)
            setDeletedObject("")
            retrieveTeams()
            retrieveApplications()
            retrieveLogs()
        }).catch((err) => {
            Notifications.error("DELETE APPLICATION FAILED", `Error occured while deleting application:\n"${err}"`)
        })
        setShowConfirmation(!showConfirmation)
    }

    // PUT - Application
    const editApplication = () => {
        let content = applicationDesc.getCurrentContent()
        const request = {
            name: teamName.trim(),
            descriptionPlainText: content.getPlainText(),
            descriptionBlocks: JSON.stringify(convertToRaw(content)),
            descriptionHtml: draftToHtml(convertToRaw(content))
        }
        Api.put(`/app/${applicationName}`, request)
        .then((response) => {
            Notifications.success("EDIT APPLICATION SUCCESS", `Successfully edit application with name "${applicationName}"!`)
            resetInput()
            retrieveApplications()
            retrieveTeams()
            retrieveLogs()
        }).catch((err) => {
            Notifications.error("EDIT APPLICATION FAILED", `Error occured while saving edited application:\n"${err}"`)
        })
        setEdit(!edit)
    }

    // FUNCTION =================================================================
    const resetInput = () => {
        if(object == "team") {
            setTeamName("")
            setTeamDesc(() => EditorState.createEmpty())
        } else {
            setApplicationName("")
            setApplicationDesc(() => EditorState.createEmpty())
        }
        setObject("")
    }

    const onConfirmDelete = (object, id) => { 
        setObject(object)
        setDeletedObject(id) 
        setShowConfirmation(!showConfirmation)
    }

    const onEditObject = (currObject, index) => {
        setObject(currObject)

        if(currObject == "team") {
            setTeamName(teams[index].name)
            setTeamDesc(EditorState.createWithContent(convertFromRaw(JSON.parse(teams[index].descriptionBlocks))))
        } else {
            setApplicationName(applications[index].name)
            setApplicationDesc(EditorState.createWithContent(convertFromRaw(JSON.parse(applications[index].descriptionBlocks))))
        }
        setEdit(!edit)
    }

    const extendAcordion = (index) => {
        if(isAccordionOpen == index) {
            return setIsAccordionOpen(null)
        }
        setIsAccordionOpen(index)
    }

    const convertFromHtml =(e) => {
        return <div dangerouslySetInnerHTML={{__html: e}}/>
    }

    const onSubmit = (e) => {
        if(e.target.name === "edit_team") {
            editTeam()
        } else if(e.target.name === "edit_app") {
            editApplication()
        } else if(e.target.name === "delete_team") {
            deleteTeam(deletedObject)
        } else if(e.target.name === "delete_app") {
            deleteApplication(deletedObject)
        }
    }

    useEffect(() => {
        const user = AuthService.getCurrentUser()
        if(user == null) return( <Redirect to="/login" /> )
        else {
            const userRole =  AuthService
                                .getCurrentUser("User")
                                .user.role.map((role, index) => {
                                    return role.roleName
                                })
            if(userRole != "Admin") return ( <Redirect to="/404" /> )
            else {
                retrieveTeams()
                retrieveApplications()
                retrieveLogs()
            }
        }
    }, [])

    return (
        <>
            <IdleTimerHandler />
            <>
                <CRow>
                    <CCol className="w-100">
                        <CInput className="form-control m-0 mb-2"
                            type="text" 
                            value={search} 
                            placeholder="Search application..." 
                            onChange={(e) => setSearch(e.target.value)}
                            style={{width: "60%"}}
                        />
                    </CCol>
                    <CCol className="text-right">
                        <CSelect className="custom-select w-50 mfs-auto" onChange={(e) => setSelectedTeam(e.target.value)}>
                            <option value=""> --- Teams in CTS-D --- </option>
                                {
                                    teams.map((team, index) => {
                                        return (
                                            <option key={team.name} value={team.name}>{team.name}</option>
                                        )
                                    })
                                }
                        </CSelect>
                    </CCol>
                    <div className="m-0 p-0 text-right mr-4 my-1">
                        <CButton className="btn-sm btn-warning"  
                            onClick={() => setShowLog(!showLog)}
                            style={{color: "white"}}>
                            Audit Log
                        </CButton>
                    </div>
                </CRow>
                <hr />

                {/* === Show Selected Team === */}
                <div className="container-fluid">
                {
                    teams.map((team, index) => {
                        if(selectedTeam == team.name) {
                            return (
                                <div key={team.name}>
                                    <CRow className="m-0 p-0">
                                        <CCol className="m-0 p-0">
                                            <h3>{team.name}</h3>
                                        </CCol>
                                        <CCol className="col-2 m-0 p-0 d-flex align-items-center justify-content-end">
                                            <CTooltip content="Edit" placement="bottom-start">
                                                <MdIcons.MdEdit className="m-1" 
                                                    onClick={() => onEditObject("team", index)}
                                                    style={{cursor: "pointer"}} 
                                                />
                                            </CTooltip>
                                            <CTooltip content="Delete" placement="bottom-start">
                                                <ImIcons.ImBin className="m-1" 
                                                    onClick={() => onConfirmDelete("team", team.name)} 
                                                    style={{cursor: "pointer"}}
                                                />
                                            </CTooltip>
                                        </CCol>
                                    </CRow>
                                    <hr />
                                    <CContainer className="m-0 p-0">
                                        <Editor
                                            editorState={EditorState.createWithContent(convertFromRaw(JSON.parse(team.descriptionBlocks)))}
                                            readOnly={true}
                                            toolbarHidden={true}
                                        />
                                    </CContainer>
                                    {
                                        team.applications.length == 0 ? 
                                        <></>:
                                        <>
                                            <br />
                                            <h5 className="mb-3">Applications managed by {team.name}</h5>
                                        </>
                                    }
                                    <div>
                                        {
                                            team.applications.filter((app, index) => {
                                                if(search=="") {
                                                    return app
                                                } else if (app.name.toLowerCase().includes(search.toLowerCase())) {
                                                    return app
                                                }
                                            }).map((app, index) => {
                                                return (
                                                    <CCol className="accordion p-0" key={app.name} xs="12" sm="12" md="12">
                                                        <CCard className="w-100 mx-0">
                                                            <CCardHeader className="accordion-header row m-0">
                                                                <CCol className="p-0">
                                                                    <CCardTitle className="m-1 ml-2" 
                                                                        onClick={() => extendAcordion(app.name)} 
                                                                        style={{fontSize: "17px", cursor: "pointer"}}>
                                                                        {app.name}
                                                                    </CCardTitle>
                                                                </CCol>
                                                                <CCol className="p-0 text-right">
                                                                    <CTooltip content="Edit" placement="bottom-start">
                                                                        <MdIcons.MdEdit className="m-1" 
                                                                            onClick={() => onEditObject("app", index)}
                                                                            style={{cursor: "pointer"}} 
                                                                        />
                                                                    </CTooltip>
                                                                    <CTooltip content="Delete" placement="bottom-start">
                                                                        <ImIcons.ImBin className="m-1" 
                                                                            onClick={() => onConfirmDelete("app", app.name)} 
                                                                            style={{cursor: "pointer"}}
                                                                        />
                                                                    </CTooltip>
                                                                    {isAccordionOpen == app.name ? 
                                                                        <RiIcons.RiArrowUpSLine className="m-1" 
                                                                            onClick={() => extendAcordion(app.name)} 
                                                                            style={{cursor: "pointer"}}
                                                                        /> :
                                                                        <RiIcons.RiArrowDownSLine className="m-1" 
                                                                            onClick={() => extendAcordion(app.name)} 
                                                                            style={{cursor: "pointer"}}
                                                                        />
                                                                    }
                                                                </CCol>
                                                            </CCardHeader>
                                                            <CCollapse className="px-3" show={isAccordionOpen == app.name}>
                                                                <CCardBody className={isAccordionOpen == app.name ? 'content show' : 'content'}>
                                                                    <Editor
                                                                        editorState={EditorState.createWithContent(convertFromRaw(JSON.parse(app.descriptionBlocks)))}
                                                                        readOnly={true}
                                                                        toolbarHidden={true}
                                                                    />
                                                                </CCardBody>
                                                            </CCollapse>
                                                        </CCard>
                                                    </CCol>
                                                )
                                            })
                                        }

                                        {/* === Modal for Edit Team/ Application === */}
                                        { <CModal size="xl" show={edit} scrollable={true} closeOnBackdrop={false}>
                                            {
                                                object == "team" ?
                                                <>
                                                    <CModalHeader>
                                                        <CModalTitle>Edit Team</CModalTitle>
                                                    </CModalHeader>
                                                    <CModalBody>
                                                        <CFormGroup>
                                                            <CInput className="form-control" 
                                                                type="text" 
                                                                value={teamName} 
                                                                readOnly
                                                            />
                                                        </CFormGroup>
                                                        <CFormGroup>
                                                            <Editor
                                                                editorState={teamDesc}
                                                                toolbarClassName="toolbar-class"
                                                                wrapperClassName="wrapper-class"
                                                                editorClassName="editor-class"
                                                                placeholder=" Write something about this team documentation..."
                                                                onEditorStateChange={setTeamDesc}
                                                            />
                                                        </CFormGroup>
                                                    </CModalBody>
                                                    <CModalFooter>
                                                        <CButton name="edit_team" color="primary" onClick={onSubmit}>Edit</CButton>
                                                        <CButton color="secondary" onClick={() => setEdit(false)}>Cancel</CButton>
                                                    </CModalFooter>
                                                </> :
                                                <>
                                                <CModalHeader>
                                                    <CModalTitle>Edit Application</CModalTitle>
                                                </CModalHeader>
                                                <CModalBody>
                                                    <CFormGroup>
                                                        <CInput className="form-control" 
                                                            type="text" 
                                                            value={applicationName} 
                                                            readOnly
                                                        />
                                                    </CFormGroup>
                                                    <CFormGroup>
                                                        <Editor
                                                            editorState={applicationDesc}
                                                            toolbarClassName="toolbar-class"
                                                            wrapperClassName="wrapper-class"
                                                            editorClassName="editor-class"
                                                            placeholder=" Write something about this application documentation..."
                                                            onEditorStateChange={setApplicationDesc}
                                                        />
                                                    </CFormGroup>
                                                </CModalBody>
                                                <CModalFooter>
                                                    <CButton name="edit_app" color="primary" onClick={onSubmit}>Edit</CButton>
                                                    <CButton color="secondary" onClick={() => setEdit(false)}>Cancel</CButton>
                                                </CModalFooter>
                                            </>
                                            }
                                        </CModal> }
                                        
                                        {/* === Modal for Action Confirmation === */}
                                        <CModal show={showConfirmation} closeOnBackdrop={false}>
                                            {
                                                object == "team" ?
                                                <>
                                                    <CModalHeader>
                                                        <CModalTitle>Delete Team</CModalTitle>
                                                    </CModalHeader>
                                                    <CModalBody>
                                                        <p>Are you sure you want to delete this team?</p>
                                                    </CModalBody>
                                                    <CModalFooter>
                                                        <CButton name="delete_team" color="success" onClick={onSubmit}>Yes</CButton>
                                                        <CButton color="danger" onClick={() => setShowConfirmation(false)}>No</CButton>
                                                    </CModalFooter>
                                                </> :
                                                <>
                                                    <CModalHeader>
                                                        <CModalTitle>Delete Application</CModalTitle>
                                                    </CModalHeader>
                                                    <CModalBody>
                                                        <p>Are you sure you want to delete this application?</p>
                                                    </CModalBody>
                                                    <CModalFooter>
                                                        <CButton name="delete_app" color="success" onClick={onSubmit}>Yes</CButton>
                                                        <CButton color="danger" onClick={() => setShowConfirmation(false)}>No</CButton>
                                                    </CModalFooter>
                                            </>
                                            }
                                        </CModal> 

                                        {/* === Modal for Audit Log === */}
                                        <CModal size="xl" show={showLog} onClose={() => setShowLog(false)} >
                                            <CModalHeader closeButton>
                                                <CModalTitle>Audit Log</CModalTitle>
                                            </CModalHeader>
                                            <CModalBody className="px-4">
                                                {
                                                    logs.length > 0 ?
                                                    <>
                                                    <MaterialTable 
                                                        columns={columns} 
                                                        data={logs} 
                                                        options={{ 
                                                            showTitle: false,
                                                            sorting: true,
                                                            search: true,
                                                            searchFieldAlignment: "left",
                                                            searchAutoFocus: true,
                                                            searchFieldVariant: "standard",
                                                            filtering: false,
                                                            paging: true,
                                                            pageSizeOptions: [5, 10, 20, 25],
                                                            pageSize: 5,
                                                            paginationType: "normal",
                                                            exportButton: true, 
                                                            exportAllData: true,
                                                            exportFileName: "[TEAM-APP]Audit_Log",
                                                            headerStyle: {
                                                                fontWeight: "bold",
                                                            },
                                                            rowStyle: (log, index) => index%2 == 0 ? {background: "#f5f5f5"} : null,
                                                        }}
                                                    />
                                                    </>  : <p className="text-center">No log yet</p>
                                                }
                                            </CModalBody>
                                        </CModal>               
                                    </div>
                                </div>
                            )
                        }
                    })
                }
                </div>
            </>

            {/* === Container for Notification === */}
            <ToastContainer />
        </>
    )
}

export default AboutViewAllDocumentations
