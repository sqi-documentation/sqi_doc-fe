import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router'

import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import { EditorState, convertToRaw } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import draftToHtml from 'draftjs-to-html'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

import {
    CCol,
    CFormGroup, CInput, CLabel, 
    CButton, CSelect, 
    CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle, 
    CCard, CCardHeader, CCardTitle, CCardBody
} from '@coreui/react'

import Api from './../../../service/Api'
import AuthService from 'src/service/AuthService'
import IdleTimerHandler from 'src/service/IdleTimer'
import Notifications from 'src/utils/Notification'

import './About.css'

const About = () => {

    // STATE ====================================================================
    // State for Action Confirmation
    const [showConfirmation, setShowConfirmation] = useState(false)

    // State for Teams
    const [teams, setTeams] = useState([])
    const [teamName, setTeamName] = useState("")
    const [teamDesc, setTeamDesc] = useState(() => EditorState.createEmpty())
    const [selectedTeam, setSelectedTeam] = useState("")

    // State for Applications
    const [applicationName, setApplicationName] = useState("")
    const [applicationDesc, setApplicationDesc] = useState(() => EditorState.createEmpty())

    // State for Identify Object (whether it's guide or attachment)
    const [object, setObject] = useState("")

    // API SERVICE ==============================================================
    // GET - Team
    const retrieveTeams = () => {
        Api.get("/team")
        .then((response) =>{
            setTeams(response.data)
        }).catch((err) => {
            Notifications.error("RETRIEVE TEAMS FAILED", `Error while retrieving teams:\n${err}`)
        })
    }

    // POST - Team
    const createTeam = () => {
        let content = teamDesc.getCurrentContent()
        const request = {
            name: teamName.trim(),
            descriptionPlainText: content.getPlainText(),
            descriptionBlocks: JSON.stringify(convertToRaw(content)),
            descriptionHtml: draftToHtml(convertToRaw(content))
        }
        // Find by ID first to check team with that name exists or not
        Api.get(`/team/${teamName}`)
        .then((isExists) => {
            if(isExists.data != null) {
                Notifications.error("CREATE TEAM FAILED", `Team with name "${teamName}" already exists!`)
            } else {
                Api.post('/team', request)
                .then((response) => {
                        Notifications.success("CREATE TEAM SUCCESS", `Successfully create team with name "${teamName}"!`)
                        resetInput()
                    } 
                ).catch((err) => {
                    Notifications.error("CREATE TEAM FAILED", `Error occured while creating team:\n${err}`)
                })
            }
        }).catch((err) => {
            Notifications.error("CREATE TEAM FAILED", `Something's wrong:\n${err}`)
        })
    }

    // POST - Application
    const createApplication = () => {
        let content = applicationDesc.getCurrentContent()
        const request = {
            name: applicationName.trim(),
            descriptionPlainText: content.getPlainText(),
            descriptionBlocks: JSON.stringify(convertToRaw(content)),
            descriptionHtml: draftToHtml(convertToRaw(content))
        }
        // Find by ID first to check application with that name exists or not
        Api.get(`/app/${applicationName}`)
        .then((isExists) => {
            if(isExists.data != null) {
                Notifications.error("CREATE APPLICATION FAILED", `Application with name "${applicationName}" already exists!`)
            } else {
                Api.post(`/app/${selectedTeam}`, request)
                .then((response) => {
                        Notifications.success("CREATE APPLICATION SUCCESS", `Successfully create application with name "${applicationName}"!`)
                        resetInput()
                        retrieveTeams()
                    } 
                ).catch((err) => {
                    Notifications.error("CREATE APP FAILED", `Error occured while creating application:\n${err}`)
                })
            }
        }).catch((err) => {
            Notifications.error("CREATE APP FAILED", `Something's wrong:\n${err}`)
        })
    }

    // FUNCTION =================================================================
    const resetInput = () => {
        if(object == "team") {
            setTeamName("")
            setTeamDesc(() => EditorState.createEmpty())
        } else {
            setSelectedTeam("")
            setApplicationName("")
            setApplicationDesc(() => EditorState.createEmpty())
        }
        setObject("")
    }

    const onSubmit = (e) => {
        if(object.length == 0) {
            Notifications.error("CREATE DOCUMENTATION ERROR", "Please check options!")
        } else {
            if(e.target.name == "add_team") {
                if (teamName.length == 0) {
                    Notifications.error("CREATE TEAM FAILED", "Team name cannot be empty!")
                } else createTeam()
            }
            else if(e.target.name == "add_app") {
                if (applicationName.length == 0) {
                    Notifications.error("CREATE APPLICATION FAILED", "Application name cannot be empty!")
                } else if(selectedTeam.length == 0) {
                    Notifications.error("CREATE APPLICATION FAILED", "Please select team!")
                }
                else createApplication()
            }
        }
        setShowConfirmation(!showConfirmation)
    }

    useEffect(() => {
        const user = AuthService.getCurrentUser()
        if(user == null) return( <Redirect to="/login" /> )
        else {
            const userRole =  AuthService
                                .getCurrentUser("User")
                                .user.role.map((role, index) => {
                                    return role.roleName
                                })
            if(userRole != "Admin") return ( <Redirect to="/404" /> )
            else retrieveTeams()
        }
    }, [])

    return (
        <>
            <IdleTimerHandler />
            <CCard className="container-fluid p-4 m-0">
                <CCardHeader className="d-flex align-items-center pt-0 px-2" 
                    style={{height: "4em"}}>
                    <CCol className="d-flex align-items-center p-0">
                        <CCardTitle className="m-0"
                            style={{fontSize: "14px"}}>
                            Options
                        </CCardTitle>
                        <CFormGroup className="form-check mx-4 m-0">
                            <input className="form-check-input" 
                                type="radio" 
                                name="flexRadioDefault" 
                                id="flexRadioDefault1" 
                                onChange={() => setObject("team")}
                            />
                            <CLabel className="form-check-label" 
                                htmlFor="flexRadioDefault1">
                                Team
                            </CLabel>
                        </CFormGroup>
                        <CFormGroup className="form-check m-0">
                            <input className="form-check-input" 
                                type="radio" 
                                name="flexRadioDefault" 
                                id="flexRadioDefault2" 
                                onChange={() => setObject("application")}
                            />
                            <CLabel className="form-check-label" 
                                htmlFor="flexRadioDefault2">
                                Application
                            </CLabel>
                        </CFormGroup>
                        {
                            object != "team" ?
                            <CCol className="col text-right p-0">
                                <CSelect className="custom-select w-50 mfs-auto" 
                                    onChange={(e) => setSelectedTeam(e.target.value)}>
                                    <option value=""> -- Select Team in CTS-D -- </option>
                                        {
                                            teams.map((team, index) => {
                                                return (
                                                    <option key={team.name} value={team.name}>{team.name}</option>
                                                )
                                            })
                                        }
                                </CSelect>
                            </CCol> : <></>
                        }
                    </CCol>
                </CCardHeader>
                <CCardBody className="p-0 pt-4">
                {
                    object == "team" ?
                        <>
                            <CFormGroup>
                                <CInput className="form-control card-header mb-3" 
                                    type="text" 
                                    value={teamName} 
                                    placeholder="Team Name" 
                                    onChange={(e) => setTeamName(e.target.value)}
                                    required
                                />
                            </CFormGroup>
                            <CFormGroup className="editor">
                                <Editor 
                                    editorState={teamDesc}
                                    toolbarClassName="toolbar-class"
                                    wrapperClassName="wrapper-class"
                                    editorClassName="editor-class"
                                    placeholder=" Write something about this team documentation..."
                                    onEditorStateChange={setTeamDesc}
                                />
                            </CFormGroup>
                        </>:
                        <>
                            <CFormGroup>
                                <CInput className="form-control card-header mb-3" 
                                    type="text" 
                                    value={applicationName} 
                                    placeholder="Application Name" 
                                    onChange={(e) => setApplicationName(e.target.value)}
                                    required
                                />
                            </CFormGroup>
                            <CFormGroup className="editor">
                                <Editor
                                    editorState={applicationDesc}
                                    toolbarClassName="toolbar-class"
                                    wrapperClassName="wrapper-class"
                                    editorClassName="editor-class"
                                    placeholder=" Write something about this application documentation..."
                                    onEditorStateChange={setApplicationDesc}
                                />
                            </CFormGroup>
                        </>
                    }    
                    <CFormGroup className="card-footer m-0 mt-4 p-0 d-flex justify-content-end">
                        <CButton color="primary" className=" mt-3" onClick={() => setShowConfirmation(!showConfirmation)}>Add</CButton>
                    </CFormGroup>
                </CCardBody>
            </CCard>

            {/* === Modal for Action Confirmation === */}
            <CModal show={showConfirmation} closeOnBackdrop={false}>
                {
                    object == "team" ?
                    <>
                    <CModalHeader>
                        <CModalTitle>Add Team Documentation</CModalTitle>
                    </CModalHeader>
                    <CModalBody>
                        <p>Are you sure you want to add this team documentation?</p>
                    </CModalBody>
                    <CModalFooter>
                        <CButton name="add_team" color="success" onClick={onSubmit}>Yes</CButton>
                        <CButton color="danger" onClick={() => setShowConfirmation(false)}>No</CButton>
                    </CModalFooter>
                    </>:
                    <>
                    <CModalHeader>
                        <CModalTitle>Add Application Documentation</CModalTitle>
                    </CModalHeader>
                    <CModalBody>
                        <p>Are you sure you want to add this application documentation?</p>
                    </CModalBody>
                    <CModalFooter>
                        <CButton name="add_app" color="success" onClick={onSubmit}>Yes</CButton>
                        <CButton color="danger" onClick={() => setShowConfirmation(false)}>No</CButton>
                    </CModalFooter>
                    </>
                }
            </CModal>
            
            {/* === Container for Notification === */}
            <ToastContainer />
        </>
    )
}

export default About
