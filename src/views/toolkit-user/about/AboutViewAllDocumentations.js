import React, { useEffect, useState } from 'react'
import { Redirect } from 'react-router'

import 'react-toastify/dist/ReactToastify.css'

import { EditorState, convertFromRaw } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

import * as RiIcons from 'react-icons/ri'

import {
    CContainer, CCol, CRow, CCollapse,
    CCard, CCardHeader, CCardBody, CCardTitle,
    CInput, 
    CSelect
} from '@coreui/react'

import Api from '../../../service/Api'
import AuthService from 'src/service/AuthService'
import IdleTimerHandler from 'src/service/IdleTimer'
import Notifications from 'src/utils/Notification'

const AboutViewAllDocumentations = () => {

    // STATE ====================================================================
    // State for Teams
    const [teams, setTeams] = useState([])
    const [selectedTeam, setSelectedTeam] = useState("")
    
    // State for Applications
    const [applications, setApplications] = useState([])
    
    // State for Search Applications
    const [search, setSearch] = useState("")
    
    // State for Expand & Collapse Application Details
    const [isAccordionOpen, setIsAccordionOpen] = useState(null)

    // API SERVICE ==============================================================
    // GET - Team
    const retrieveTeams = () => {
        Api.get("/team")
        .then((response) => {
            setTeams(response.data)
        }).catch((err) => {
            Notifications.error("RETRIEVE TEAMS FAILED", `Error while retrieving teams:\n${err}`)
        })
    }

    // GET - Applications
    const retrieveApplications = () => {
        Api.get("/app")
        .then((response) => {
            setApplications(response.data)
        }).catch((err) => {
            Notifications.error("RETRIEVE APPLICATIONS FAILED", `Error while retrieving applications:\n${err}`)
        })
    }

    // FUNCTION =================================================================
    const extendAcordion = (index) => {
        if(isAccordionOpen == index) {
            return setIsAccordionOpen(null)
        }
        setIsAccordionOpen(index)
    }

    useEffect(() => {
        const user = AuthService.getCurrentUser()
        if(user == null) return( <Redirect to="/login" /> )
        else {
            const userRole =  AuthService
                                .getCurrentUser("User")
                                .user.role.map((role, index) => {
                                    return role.roleName
                                })
            if(userRole != "User") return ( <Redirect to="/404" /> )
            else {
                retrieveTeams()
                retrieveApplications()
            }
        }
    }, [])

    return (
        <>
            <IdleTimerHandler />
            <>
                <CRow>
                    <CCol className="w-100">
                        <CInput className="form-control m-0 mb-2"
                            type="text" 
                            value={search} 
                            placeholder="Search application..." 
                            onChange={(e) => setSearch(e.target.value)}
                            style={{width: "60%"}}
                        />
                    </CCol>
                    <CCol className="text-right">
                        <CSelect className="custom-select w-50 mfs-auto" onChange={(e) => setSelectedTeam(e.target.value)}>
                            <option value=""> --- Teams in CTS-D --- </option>
                                {
                                    teams.map((team, index) => {
                                        return (
                                            <option key={team.name} value={team.name}>{team.name}</option>
                                        )
                                    })
                                }
                        </CSelect>
                    </CCol>
                </CRow>
                <hr />

                {/* === Show Selected Team === */}
                <div className="container-fluid">
                {
                    teams.map((team, index) => {
                        if(selectedTeam == team.name) {
                            return (
                                <div key={team.name}>
                                    <CRow className="m-0 p-0">
                                        <CCol className="m-0 p-0">
                                            <h3>{team.name}</h3>
                                        </CCol>
                                    </CRow>
                                    <hr />
                                    <CContainer className="m-0 p-0">
                                        <Editor
                                            editorState={EditorState.createWithContent(convertFromRaw(JSON.parse(team.descriptionBlocks)))}
                                            readOnly={true}
                                            toolbarHidden={true}
                                        />
                                    </CContainer>
                                    {
                                        team.applications.length == 0 ? 
                                        <></>:
                                        <>
                                            <br />
                                            <h5 className="mb-3">Applications managed by {team.name}</h5>
                                        </>
                                    }
                                    <div>
                                        {
                                            team.applications.filter((app, index) => {
                                                if(search=="") {
                                                    return app
                                                } else if (app.name.toLowerCase().includes(search.toLowerCase())) {
                                                    return app
                                                }
                                            }).map((app, index) => {
                                                return (
                                                    <CCol className="accordion p-0" key={app.name} xs="12" sm="12" md="12">
                                                        <CCard className="w-100 mx-0">
                                                            <CCardHeader className="accordion-header row m-0">
                                                                <CCol className="p-0">
                                                                    <CCardTitle className="m-1 ml-2" 
                                                                        onClick={() => extendAcordion(app.name)} 
                                                                        style={{fontSize: "17px", cursor: "pointer"}}>
                                                                        {app.name}
                                                                    </CCardTitle>
                                                                </CCol>
                                                                <CCol className="p-0 text-right">
                                                                    {isAccordionOpen == app.name ? 
                                                                        <RiIcons.RiArrowUpSLine className="m-1" 
                                                                            onClick={() => extendAcordion(app.name)} 
                                                                            style={{cursor: "pointer"}}
                                                                        /> :
                                                                        <RiIcons.RiArrowDownSLine className="m-1" 
                                                                            onClick={() => extendAcordion(app.name)} 
                                                                            style={{cursor: "pointer"}}
                                                                        />
                                                                    }
                                                                </CCol>
                                                            </CCardHeader>
                                                            <CCollapse className="px-3" show={isAccordionOpen == app.name}>
                                                                <CCardBody className={isAccordionOpen == app.name ? 'content show' : 'content'}>
                                                                    <Editor
                                                                        editorState={EditorState.createWithContent(convertFromRaw(JSON.parse(app.descriptionBlocks)))}
                                                                        readOnly={true}
                                                                        toolbarHidden={true}
                                                                    />
                                                                </CCardBody>
                                                            </CCollapse>
                                                        </CCard>
                                                    </CCol>
                                                )
                                            })
                                        }           
                                    </div>
                                </div>
                            )
                        }
                    })
                }
                </div>
            </>
        </>
    )
}

export default AboutViewAllDocumentations
