import React, { useEffect, useState } from 'react'
import { Redirect } from 'react-router'

import 'react-toastify/dist/ReactToastify.css'

import { EditorState, convertToRaw, convertFromRaw } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

import * as RiIcons from 'react-icons/ri'

import {
    CCol, CRow, CCollapse,
    CCard, CCardHeader, CCardBody, CCardTitle,
    CInput, 
    CButton,
} from '@coreui/react'

import Api from './../../../service/Api'
import AuthService from 'src/service/AuthService'
import IdleTimerHandler from 'src/service/IdleTimer'
import Notifications from 'src/utils/Notification'

const WikiViewAllGuides = () => {
    
    // STATE ====================================================================
    // State for Guide
    const [guides, setGuides] = useState([])
    
    // State for Search Guide
    const [search, setSearch] = useState("")

    // State for Expand & Collapse Guide Details
    const [isAccordionOpen, setIsAccordionOpen] = useState(null)

    // API SERVICE ==============================================================
    // GET - Guide
    const retrieveGuides = () => {
        Api.get("/guide")
        .then((response) => {
            setGuides(response.data)
        }).catch((err) => {
            Notifications.error("RETRIEVE GUIDES FAILED", `Error while retrieving guides:\n${err}`)
        })
    }

    // FUNCTION =================================================================
    const extendAcordion = (index) => {
        if(isAccordionOpen == index) {
            return setIsAccordionOpen(null)
        }
        setIsAccordionOpen(index)
    }

    useEffect(() => {
        const user = AuthService.getCurrentUser()
        if(user == null) return( <Redirect to="/login" /> )
        else {
            const userRole =  AuthService
                                .getCurrentUser("User")
                                .user.role.map((role, index) => {
                                    return role.roleName
                                })
            if(userRole != "User") return ( <Redirect to="/404" /> )
            else {
                retrieveGuides()
            }
        }
    }, [])

    return (
        <>
            <IdleTimerHandler />
            <>
                <CRow>
                    <CCol className="w-100">
                        <CInput className="form-control m-0 mb-2"
                            type="text" 
                            value={search} 
                            placeholder="Search guide ..."
                            onChange={(e) => setSearch(e.target.value)}
                            style={{width: "30%"}} 
                        />
                    </CCol>
                </CRow>
                <hr />

                {/* === List of Guides === */}
                {
                    guides.filter((guide, index) => {
                        if(search=="") {
                            return guide
                        } else if (guide.title.toLowerCase().includes(search.toLowerCase())) {
                            return guide
                        }
                    }).map((guide, index) => {
                        return (
                            <CCol className="accordion p-0" key={guide.title} xs="12" sm="12" md="12">
                                <CCard className="w-100 px-2">
                                    <CCardHeader className="accordion-header row">
                                        <CCol className="p-0">
                                            <CCardTitle className="m-1 ml-2"
                                                onClick={() => extendAcordion(guide.title)}
                                                style={{fontSize: "17px", cursor:"pointer"}}>
                                                {guide.title}
                                            </CCardTitle>
                                        </CCol>
                                        <CCol className="p-0 text-right">
                                            {
                                                isAccordionOpen == guide.title ? 
                                                <RiIcons.RiArrowUpSLine className="m-1" 
                                                    onClick={() => extendAcordion(guide.title)} 
                                                    style={{cursor: "pointer"}}
                                                /> :
                                                <RiIcons.RiArrowDownSLine className="m-1" 
                                                    onClick={() => extendAcordion(guide.title)} 
                                                    style={{cursor: "pointer"}}
                                                />
                                            }
                                        </CCol>
                                    </CCardHeader>
                                    <CCollapse show={isAccordionOpen == guide.title}>
                                        <CCardBody className={isAccordionOpen == guide.title ? 'content show' : 'content'}>
                                            <Editor
                                                editorState={EditorState.createWithContent(convertFromRaw(JSON.parse(guide.contentBlocks)))}
                                                readOnly={true}
                                                toolbarHidden={true}
                                            />
                                        </CCardBody>
                                    </CCollapse>
                                </CCard>
                            </CCol>
                        )
                    })
                }
            </>
        </>
    )
}

export default WikiViewAllGuides
