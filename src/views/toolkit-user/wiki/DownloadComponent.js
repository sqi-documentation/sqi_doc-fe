import React, { Component } from 'react'

import {
  CLink
} from  '@coreui/react'
import CIcon from '@coreui/icons-react'

import FileService from 'src/service/FileService';

class DownloadComponent extends Component {
    
    _isMounted = false

    constructor(props) {
        super(props);

        this.state = {
          search: "",
    
          files: [],
        };
    }

    searchFile(event) {
        this.setState({
          search: event.target.value
        });
    }

    componentDidMount() {

        this._isMounted = true;

        FileService.getFiles().then((response) => {
          if(this._isMounted) {
            this.setState({
                files: response.data,
              });
          }
        })
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {
        const {
          files,
          search
        } = this.state;
    
        return (
        <>
            <div>
                <input type="text" value={search} placeholder="Search template..." className="form-control ml-3 w-25 mb-4" onChange={(e) => this.searchFile(e)} />
            </div>
            <hr />
            <div className="card mx-3">
                <div className="card-header">
                    <h4 className="card-title m-0 my-1">List of Templates</h4>
                </div>
              <ul className="list-group list-group-flush">
                {files &&
                  files.filter((file, index) => {
                    if(search=="") {
                        return file
                    } else if (file.name.toLowerCase().includes(search.toLowerCase())) {
                        return file
                    }
                }).map((file, index) => (
                    <li className="list-group-item d-flex align-items-center" key={index}>
                        <CLink className="card-header-action" name="download_attach" href={file.url} target="_blank">
                          <CIcon name="cil-cloud-download"/>{' '}
                          {' '}{file.name}
                        </CLink>
                    </li>
                  ))}
              </ul>
            </div>
          </>
        );
      }
    }

export default DownloadComponent
