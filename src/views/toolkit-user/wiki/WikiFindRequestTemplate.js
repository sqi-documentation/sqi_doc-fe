import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router'

import AuthService from 'src/service/AuthService'
import IdleTimerHandler from 'src/service/IdleTimer';
import DownloadComponent from './DownloadComponent';

const WikiFindRequestTemplate = () => {


    if(AuthService.getCurrentUser() == null) {
        return( <Redirect to="/login" /> )
    }

    const who = AuthService.getCurrentUser("User").user.role.map((role, index) => {
        return role.roleName
    })

    // console.log("Current user role  [WikiUser-Request]: " + who)
    
    if(who == "Admin") {
        return ( <Redirect to="/404" /> )
    }

    return (
        <>
            <IdleTimerHandler />
            <DownloadComponent />
        </>
    )
}

export default WikiFindRequestTemplate
