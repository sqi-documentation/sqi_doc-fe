import React, { useState } from 'react'
import { useHistory } from 'react-router'

import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';

import {
  CContainer, CCol, CRow,
  CCard, CCardBody, CCardGroup,
  CForm, CInput, CInputGroup, CInputGroupPrepend, CInputGroupText,
  CButton
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

import AuthService from 'src/service/AuthService'
import Notifications from 'src/utils/Notification'

const Login = () => {

  const history = useHistory()

  // STATE ====================================================================
  // State for Login  
  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")

  // FUNCTION =================================================================
  const handleLogin = (e) => {
    e.preventDefault()

    AuthService.login(username, password)
    .then((response) => {
        history.push('/')
    },(error) => {
        setUsername("")
        setPassword("")
        Notifications.error("LOGIN FAILED", "Please enter username and password correctly!")
    })
  }

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="5">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput type="text" 
                              placeholder="Username" 
                              autoComplete="username" 
                              id="username"
                              name="username" 
                              value={username}
                              onChange={(e) => setUsername(e.target.value)}
                              required />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput type="password" 
                              placeholder="Password" 
                              autoComplete="current-password" 
                              id="password" 
                              name="password" 
                              value={password}
                              onChange={(e) => setPassword(e.target.value)}/>
                    </CInputGroup>
                    <CRow>
                      <CCol xs="6" className="text-left">
                        {/* <CButton color="link" className="px-0">Forgot password?</CButton> */}
                      </CCol>
                      <CCol xs="6" className="text-right">
                        <CButton className="px-4"
                              color="primary" 
                              disabled={username.length == 0 || password.length == 0}
                              onClick={handleLogin.bind(this)}>
                              Login
                        </CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
              {/* <CCard className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                <CCardBody className="text-center">
                  <div>
                    <h2>Sign up</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                      labore et dolore magna aliqua.</p>
                    <Link to="/register">
                      <CButton color="primary" className="mt-3" active tabIndex={-1}>Register Now!</CButton>
                    </Link>
                  </div>
                </CCardBody>
              </CCard> */}
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>

      {/* === Container for Notification === */}
      <ToastContainer />
    </div>
  )
}

export default Login
