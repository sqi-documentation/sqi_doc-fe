import React from 'react'
import { Redirect } from 'react-router'
import { Link } from 'react-router-dom'

import { 
    CCol,CRow,
    CCard, CCardBody,
    CFade 
} from '@coreui/react'

import BCAlogo from './../../../assets/images/logo-bca.png'
import MailLogo from './../../../assets/images/logo-mail.png'
import WikiLogo from './../../../assets/images/layers-fill.svg'
import AboutLogo from './../../../assets/images/alert-circle.svg'

import TheFooter from './../../../containers/TheFooter'
import AuthService from 'src/service/AuthService'
import IdleTimerHandler from 'src/service/IdleTimer'

const Home = () => {

    const handleLogout = (e) => {
        window.location.reload()
        window.setInterval(AuthService.logout(), 500)
    }

    const user = AuthService.getCurrentUser()
    if(user == null) return( <Redirect to="/login" /> )
    
    // console.log('home:' + AuthService.getCurrentUser())

    return (
        <>
        <IdleTimerHandler />
        <CFade>
            <div className="c-app c-default-layout">
                <div className="c-wrapper">
                    <header className="bg-white shadow d-flex align-items-center" 
                        style={{height: "5rem"}}>
                        <img className="ml-2" 
                            src={BCAlogo} 
                            style={{height: "180%"}} />
                        <div className="mfs-auto">
                            <h6 className="m-0 mr-4" 
                                onClick={handleLogout.bind(this)} 
                                style={{color: "#E00C0C", cursor: "pointer"}}>
                                Log Out
                            </h6>
                        </div>
                    </header>
                    <div className="c-body">
                        <CRow className="mx-2 my-1">
                            <CCard className="shadow rounded m-4 me-0" 
                                style={{height:"10rem", width: "13rem", backgroundColor: "#4471B4"}}>
                                <Link to="/mail" style={{textDecorationLine: "none", color: "white"}}>
                                    <CCardBody className="mt-3">
                                        <div className="m-2 d-flex justify-content-center align-items-center">
                                            <img src={MailLogo} style={{height: "2.4rem"}}/>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                            <h5 className="card-title m-0">Mail</h5>
                                        </div>
                                    </CCardBody>
                                </Link>
                            </CCard>
                            {
                                AuthService.getCurrentUser("User").user.role.map((role, index) => {
                                    if(role.roleName == "Admin") {
                                        return (
                                            <CCard className="shadow rounded m-4 me-0" 
                                                key={index} 
                                                style={{height:"10rem", width: "13rem", backgroundColor: "#773696"}}>
                                                <Link to="/wiki" style={{textDecorationLine: "none", color: "white"}}>
                                                    <CCardBody className="mt-3">
                                                        <div className="m-2 d-flex justify-content-center align-items-center">
                                                            <img src={WikiLogo} />
                                                        </div>
                                                        <div className="d-flex justify-content-center align-items-center">
                                                            <h5 className="card-title m-0">Wiki</h5>
                                                        </div>
                                                    </CCardBody>
                                                </Link>
                                            </CCard>
                                        )
                                    } else {
                                        return (
                                            <CCard className="shadow rounded m-4 me-0" 
                                                key={index} 
                                                style={{height:"10rem", width: "13rem", backgroundColor: "#773696"}}>
                                                <Link to="/wiki-user" style={{textDecorationLine: "none", color: "white"}}>
                                                    <CCardBody className="mt-3">
                                                        <div className="m-2 d-flex justify-content-center align-items-center">
                                                            <img src={WikiLogo} />
                                                        </div>
                                                        <div className="d-flex justify-content-center align-items-center">
                                                            <h5 className="card-title m-0">Wiki</h5>
                                                        </div>
                                                    </CCardBody>
                                                </Link>
                                            </CCard>
                                        )
                                    }
                                })
                            }
                            {
                             AuthService.getCurrentUser("User").user.role.map((role, index) => {
                                    if(role.roleName == "Admin") {
                                        return (
                                            <CCard className="shadow rounded m-4 me-0" 
                                                key={index} 
                                                style={{height:"10rem", width: "13rem", backgroundColor: "#B93A3A"}}>
                                                <Link to="/about" style={{textDecorationLine: "none", color: "white"}}>
                                                    <CCardBody className="mt-3">
                                                        <div className="m-2 d-flex justify-content-center align-items-center">
                                                            <img src={AboutLogo} />
                                                        </div>
                                                        <div className="d-flex justify-content-center align-items-center">
                                                            <h5 className="card-title m-0">About</h5>
                                                        </div>
                                                    </CCardBody>
                                                </Link>
                                            </CCard>
                                        )
                                    } else {
                                        return (
                                            <CCard className="shadow rounded m-4 me-0" 
                                                key={index} 
                                                style={{height:"10rem", width: "13rem", backgroundColor: "#B93A3A"}}>
                                                <Link to="/about-user/viewalldocumentations" style={{textDecorationLine: "none", color: "white"}}>
                                                    <CCardBody className="mt-3">
                                                        <div className="m-2 d-flex justify-content-center align-items-center">
                                                            <img src={AboutLogo} />
                                                        </div>
                                                        <div className="d-flex justify-content-center align-items-center">
                                                            <h5 className="card-title m-0">About</h5>
                                                        </div>
                                                    </CCardBody>
                                                </Link>
                                            </CCard>
                                        )
                                    }
                                })
                            }
                        </CRow>
                    </div>
                    <TheFooter />
                </div>
            </div>
        </CFade>
        </>
    )
}

export default Home
