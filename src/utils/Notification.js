import { toast, Slide } from 'react-toastify'

const message = (title, description) => {
    return (
        <div className="p-0 m-2">
            <h6 style={{fontWeight: "bolder", marginBottom: "2px"}}>{title}</h6>
            <p style={{fontSize: "12px", margin: "0px"}}>{description}</p>
        </div>
    )
}

const success = (title, description) => {
    
    toast.success(message(title, description), {
        position: toast.POSITION.BOTTOM_RIGHT,
        theme: 'colored',
        autoClose: 3000,
        draggable: false,
        closeButton: false,
        pauseOnFocusLoss: false,
        transition: Slide
    })
}

const error = (title, description) => {
    
    toast.error(message(title, description), {
        position: toast.POSITION.BOTTOM_RIGHT,
        theme: 'colored',
        autoClose: 3000,
        draggable: false,
        closeButton: false,
        pauseOnFocusLoss: false,
        transition: Slide
    })
}

const Notifications = {
    success,
    error
}

export default Notifications