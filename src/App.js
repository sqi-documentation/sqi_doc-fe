import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import './scss/style.scss';

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

// Containers
const TheLayout = React.lazy(() => import('./containers/TheLayout'));
const TheLayout2 = React.lazy(() => import('./containers/TheLayout2'));
const TheLayoutUser = React.lazy(() => import('./containers/toolkit-user/TheLayout'));
const TheLayout2User = React.lazy(() => import('./containers/toolkit-user/TheLayout2'));

// Pages
const Login = React.lazy(() => import('./views/pages/login/Login'));
const Register = React.lazy(() => import('./views/pages/register/Register'));
const Page404 = React.lazy(() => import('./views/pages/page404/Page404'));
const Page500 = React.lazy(() => import('./views/pages/page500/Page500'));
const Home = React.lazy(() => import('./views/pages/home/Home'));
const Mail = React.lazy(() => import('./views/toolkit/mail/Mail'));

class App extends Component {

  render() {
    return (
      <BrowserRouter>
          <React.Suspense fallback={loading}>
            <Switch>
                <Route exact path="/login" name="Login Page" render={props => <Login {...props}/>} />
                {/* <Route exact path="/register" name="Register Page" render={props => <Register {...props}/>} /> */}
                <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
                <Route exact path="/500" name="Page 500" render={props => <Page500 {...props}/>} />
                <Route path="/mail" name="Mail" component={props => <Mail {...props}/>} />
                <Route path="/wiki" name="Wiki" component={props => <TheLayout {...props}/>} />
                <Route path="/about" name="About" component={props => <TheLayout2 {...props}/>} />
                <Route path="/wiki-user" name="Wiki-User" component={props => <TheLayoutUser {...props}/>} />
                <Route path="/about-user" name="About-User" component={props => <TheLayout2User {...props}/>} />
                <Route exact path="/" name="Home" component={props => <Home {...props}/>} />
                <Route path="*" component={() => <Redirect to="/500"/>} />
            </Switch>
          </React.Suspense>
      </BrowserRouter>
    );
  }
}

export default App;
