import axios from "axios";
import AuthHeader from "./AuthHeader";

export default axios.create({
    baseURL: "http://localhost:8888/api",
    // baseURL: "https://toolkit-be.herokuapp.com/api",
    headers: AuthHeader()
})