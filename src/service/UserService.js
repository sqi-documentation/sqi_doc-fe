import axios from "axios"
import authHeader from './AuthHeader'

const API_AUTH_URL = "http://localhost:8888"

class UserService {

    getUserSession() {
       return axios.get(API_AUTH_URL + "/api/user/forUser", { headers: authHeader() })
    }

    getAdminSession() {
        return axios.get(API_AUTH_URL + "/api/user/forAdmin", { headers: authHeader() })
    }
}

export default new UserService()