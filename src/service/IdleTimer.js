import React, { useRef } from 'react'
import { useHistory } from 'react-router'
import IdleTimer from 'react-idle-timer'
import AuthService from './AuthService'


const IdleTimerHandler = () => {

    const idleTimerRef = useRef(null)
    const history = useHistory()

    const onIdle = () => {
        console.log("Idle!")
        AuthService.logout()
        history.push("/login")
    }

    return (
        <IdleTimer 
            ref={idleTimerRef}
            timeout={3600 * 1 * 1000}
            onIdle={onIdle}
        />
    )
}

export default IdleTimerHandler
