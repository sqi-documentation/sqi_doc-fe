function AuthHeader() {
    
    const user = JSON.parse(localStorage.getItem("User"))

    if(user && user.token) {
        return {
            Authorization: 'Bearer ' + user.token
        }
    } else {
        return {}
    }
}

export default AuthHeader
