import axios from "axios"

const API_AUTH_URL = "http://localhost:8888"

class AuthService {

    login(username, password) {
        return axios.post(API_AUTH_URL + "/authenticate", {
            username,
            password
        }).then(response => {

            if(response.data.token) {
                localStorage.setItem("User", JSON.stringify(response.data))
            }

            return response.data;
        })
    }

    logout() {
        localStorage.removeItem("User");
    }

    getCurrentUser() {
        return JSON.parse(localStorage.getItem("User"))
    }
}

export default new AuthService()