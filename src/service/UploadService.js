import Api from './Api'

class UploadService {
   
    upload(file, onUploadProgress) {
        let formData = new FormData()
        formData.append("file", file)

        return Api.post("/upload", formData, onUploadProgress)
    }

    getFiles() {
        return Api.get("/files")
    }

    download(id) {
        return Api.get(`/files/${id}`)
    }
}

export default new UploadService()