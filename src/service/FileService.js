import axios from "axios"
import Api from "./Api";

const upload = (file, onUploadProgress) => {

    let formData = new FormData()
    const user = JSON.parse(localStorage.getItem("User"))

    formData.append("file", file);
    return axios.post('http://localhost:8888/api/upload', formData, {
        headers: {
            "Authorization": 'Bearer ' + user.token,
            "Content-Type": "multipart/form-data",
        },
        onUploadProgress,
    })
}

const getFiles = () => {
    // return Api.get(`/files/${guide_id}`)
    return Api.get('/files')
}

const FileService = {
    upload,
    getFiles
}

export default FileService