import React from 'react'
import {
  TheContent2User,
  TheSidebar2User,
  TheFooterUser,
  TheHeaderUser
} from './../index'

const TheLayout2 = () => {

  return (
    <div className="c-app c-default-layout">
      <TheSidebar2User/>
      <div className="c-wrapper">
        <TheHeaderUser/>
        <div className="c-body">
          <TheContent2User/>
        </div>
        <TheFooterUser/>
      </div>
    </div>
  )
}

export default TheLayout2
