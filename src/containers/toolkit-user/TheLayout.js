import React from 'react'
import {
  TheContentUser,
  TheSidebarUser,
  TheFooterUser,
  TheHeaderUser
} from './../index'

const TheLayout = () => {

  return (
    <div className="c-app c-default-layout">
      <TheSidebarUser/>
      <div className="c-wrapper">
        <TheHeaderUser/>
        <div className="c-body">
          <TheContentUser />
        </div>
        <TheFooterUser/>
      </div>
    </div>
  )
}

export default TheLayout
