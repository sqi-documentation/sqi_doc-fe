import React from 'react'
import {
  TheContent2,
  TheSidebar2,
  TheFooter,
  TheHeader
} from './index'

const TheLayout2 = () => {

  return (
    <div className="c-app c-default-layout">
      <TheSidebar2/>
      <div className="c-wrapper">
        <TheHeader/>
        <div className="c-body">
          <TheContent2/>
        </div>
        <TheFooter/>
      </div>
    </div>
  )
}

export default TheLayout2
